## Contents of this code package

This is code accompanying my master thesis, included is:
- a subset of the experiments code, which can serve as examples on how to use the Celerite2, S+Leaf and GPyTorch 
  libraries for GP regression
- reusable code in src/clip_windows.py for the eclipse masking and experiment_util.py for experiment setup and evaluation
- notebooks that demonstrate the simulations (chapter 5)

I have also included the data for KIC 11285625 and for the simulation light curves.
 
## How to run the experiments and the Jupyter notebooks?

Requirements:
Python 3.8+
Linux or Mac OS X assumed

1. Create a virtual environment
   virtualenv venv
   source venv/bin/activate

2. Install all necessary packages
   pip3 install numpy
   pip3 install -r requirements.txt
   
3. To run an experiment:
   PYTHONPATH=src python3 src/extract_eb_celerite_matern_experiment.py
 
    All experiments will store the results as Excel files in the results/ directory

4. To run a notebook:
   jupyter notebook
    
   Browse to the notebooks folder, open one of the notebooks and Run.
  