from abc import ABC, abstractmethod

from matplotlib import pyplot as plt
import numpy as np
from astropy.timeseries import LombScargle
import torch

FONT_SIZE=20
PLOT_WIDTH=8
PlOT_HEIGHT=3
# FONT_SIZE=26
# PLOT_WIDTH=35
# PlOT_HEIGHT=5

class PlotUtil(ABC):
    @abstractmethod
    def plot_kernel_samples(self, cov_matrix, nr_of_data_points=None, nr_of_samples=5):
        pass

    def plot_data(self, X, Y, X2=None, Y2=None, fig_width=PLOT_WIDTH, fig_height=PlOT_HEIGHT, fmt='.', fmt2='.'):
        # Setup our figure environment
        plt.figure(figsize=(fig_width, fig_height))

        # Plot observations
        plt.plot(X, Y, fmt, mew=1, linewidth=1, mec="black")
        if X2 is not None:
            plt.plot(X2, Y2, fmt2, mew=1, linewidth=1, mec="red")

        # Annotate plot
        plt.xticks(fontsize=FONT_SIZE), plt.yticks(fontsize=FONT_SIZE)
        plt.xlabel("t", fontsize=FONT_SIZE), plt.ylabel("f", fontsize=FONT_SIZE)

    def plot_series(self, data_x, data_y, smooth_x=None, smooth_y=None, var=None, fig_height=PlOT_HEIGHT):
        # Plot
        plt.figure(figsize=(PLOT_WIDTH, fig_height))
        plt.plot(data_x, data_y, ".", mew=1, linewidth=1, mec="black", label='truth')
        # plt.plot(tsmooth, ysignal, 'r', label='truth')
        # plt.errorbar(t, y, yerr, fmt='.', color='k', label='meas.')
        if not smooth_x is None:
            plt.plot(smooth_x, smooth_y, 'r', label='predict.')
            if not var is None:
                plt.fill_between(smooth_x, smooth_y - np.sqrt(var),
                                 smooth_y + np.sqrt(var), color='g', alpha=0.5)

        plt.xticks(fontsize=FONT_SIZE), plt.yticks(fontsize=FONT_SIZE)
        plt.xlabel('t', fontsize=FONT_SIZE), plt.ylabel('f', fontsize=FONT_SIZE)

    def plot_lomb_scargle_periodogram(self, x, y, min_freq, max_freq, frequencies=None):
        if frequencies is not None:
            power = LombScargle(x, y).power(frequencies)
        else:
            frequencies, power = LombScargle(x, y).autopower(minimum_frequency=min_freq,
                                                             maximum_frequency=max_freq)
        plt.figure(figsize=(PLOT_WIDTH, PlOT_HEIGHT))
        plt.xticks(fontsize=FONT_SIZE), plt.yticks(fontsize=FONT_SIZE)
        plt.xlabel('frequency', fontsize=FONT_SIZE), plt.ylabel('power', fontsize=FONT_SIZE)
        plt.plot(frequencies, power)
        return frequencies, power

    @abstractmethod
    def plot_1d_kernel(self, kernel, label="sample points", ax=None):
        pass

    @abstractmethod
    def plot_2d_kernel(self, kernel, ax=None):
        pass


class PlotUtilGPyTorch(PlotUtil):
    def plot_kernel_samples(self, model, nr_of_data_points=None, nr_of_samples=5):
        raise NotImplementedError()

    def plot_1d_kernel(self, kernel, label="sample points", ax=None):
        # Predict evenly spaced points on our time-axis
        x = np.linspace(-6, 6, 5000)
        baseline = np.zeros(5000)
        with torch.no_grad():
            C = kernel(torch.tensor(x), torch.tensor(baseline)).evaluate()
            C = C.numpy()

        if ax is None:
            # Setup our figure environment
            fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PlOT_HEIGHT))

        # Plot observations
        ax.plot(x, C[:, 1], ".", mew=1, linewidth=1, mec="black")

        # Annotate plot
        ax.set_ylabel('y', fontsize=FONT_SIZE)  # Y label
        ax.set_xlabel('t', fontsize=FONT_SIZE)  # X label
        ax.tick_params(axis='both', which='both', labelsize=FONT_SIZE)

    def plot_2d_kernel(self, kernel, fig=None, ax=None, ticks=[]):
        # Our sample space
        x = np.linspace(-5., 5., 250)
        with torch.no_grad():
            C = kernel(torch.tensor(x), torch.tensor(x)).evaluate()
            C = C.numpy()

        if ax is None:
            # Setup our figure environment
            fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PlOT_HEIGHT))

        # Visualise covariance matrix on right hand side
        plt.pcolor(x.T, x, C)
        # Annotate plot
        plt.gca().invert_yaxis(), plt.gca().axis("image")
        cbar = plt.colorbar() #ticks=[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7])
        ax.set_ylabel("t'", fontsize=FONT_SIZE)  # Y label
        ax.set_xlabel('t', fontsize=FONT_SIZE)  # X label
        cbar.ax.get_yaxis().set_ticks(ticks)
        ax.tick_params(axis='both', which='both', labelsize=FONT_SIZE)
        cbar.ax.set_yticklabels(ticks, fontdict={'fontsize': FONT_SIZE})


class PlotUtilSPLeaf(PlotUtil):
    def plot_kernel_samples(self, cov_matrix, nr_of_data_points=None, nr_of_samples=5):
        plt.figure(figsize=(PLOT_WIDTH, PlOT_HEIGHT))
        Y_sample = cov_matrix.sample(nr_of_samples)
        if nr_of_data_points is None:
            nr_of_data_points = cov_matrix.W.shape[0]
        for i in range(nr_of_samples):
            plt.plot(range(nr_of_data_points), Y_sample[i])

    @staticmethod
    def __eval_2d(x):
        return np.array([x - i for i in x.tolist()])

    def plot_2d_kernel(self, kernel, fig=None, ax=None, ticks=[]):
        # Our sample space
        x = np.linspace(-5., 5., 250)
        grid = PlotUtilSPLeaf.__eval_2d(x)
        C = kernel.eval(grid)

        if ax is None:
            # Setup our figure environment
            fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PlOT_HEIGHT))

        # Visualise covariance matrix on right hand side
        plt.pcolor(x.T, x, C)
        # Annotate plot
        plt.gca().invert_yaxis(), plt.gca().axis("image")
        cbar = plt.colorbar() #ticks=[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7])
        ax.set_ylabel("t'", fontsize=FONT_SIZE)  # Y label
        ax.set_xlabel('t', fontsize=FONT_SIZE)  # X label
        cbar.ax.get_yaxis().set_ticks(ticks)
        ax.tick_params(axis='both', which='both', labelsize=FONT_SIZE)
        cbar.ax.set_yticklabels(ticks, fontdict={'fontsize': FONT_SIZE})

    def plot_1d_kernel(self, kernel, label="sample points", ax=None):
        if ax is None:
            # Setup our figure environment
            fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PlOT_HEIGHT))

        # Predict evenly spaced points on our time-axis
        x = np.linspace(-6, 6, 1000)
        y = kernel.eval(x)

        # Plot observations
        ax.plot(x, y, ".", mew=1, linewidth=1, mec="black")

        # Annotate plot
        ax.set_ylabel('y', fontsize=FONT_SIZE)  # Y label
        ax.set_xlabel('t', fontsize=FONT_SIZE)  # X label
        ax.tick_params(axis='both', which='both', labelsize=FONT_SIZE)


class PlotUtilCelerite(PlotUtil):
    def plot_kernel_samples(self, cov_matrix, nr_of_data_points=None, nr_of_samples=5):
        Y_sample = cov_matrix.sample(size=nr_of_data_points)
        if nr_of_data_points is None:
            nr_of_data_points = cov_matrix.W.shape[0]
        for i in range(nr_of_samples):
            plt.plot(range(nr_of_data_points), Y_sample[i])

    def plot_1d_kernel(self, kernel, label="sample points", ax=None):
        if ax is None:
            # Setup our figure environment
            fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PlOT_HEIGHT))


        # Predict evenly spaced points on our time-axis
        x = np.linspace(-6, 6, 1000)
        y = kernel.get_value(x)

        # Plot observations
        ax.plot(x, y, ".", mew=1, linewidth=1, mec="black")

        # Annotate plot
        ax.set_ylabel('y', fontsize=FONT_SIZE)  # Y label
        ax.set_xlabel('t', fontsize=FONT_SIZE)  # X label
        ax.tick_params(axis='both', which='both', labelsize=FONT_SIZE)

    @staticmethod
    def __eval_2d(x):
        return np.array([x - i for i in x.tolist()])

    def plot_2d_kernel(self, kernel, fig=None, ax=None, ticks=[]):
        # Our sample space
        x = np.linspace(-5., 5., 250)
        grid = PlotUtilCelerite.__eval_2d(x)
        C = kernel.get_value(grid)

        if ax is None:
            # Setup our figure environment
            fig, ax = plt.subplots(figsize=(PLOT_WIDTH, PlOT_HEIGHT))

        # Visualise covariance matrix on right hand side
        plt.pcolor(x.T, x, C)
        # Annotate plot
        plt.gca().invert_yaxis(), plt.gca().axis("image")
        cbar = plt.colorbar()  # ticks=[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7])
        ax.set_ylabel("t'", fontsize=FONT_SIZE)  # Y label
        ax.set_xlabel('t', fontsize=FONT_SIZE)  # X label
        cbar.ax.get_yaxis().set_ticks(ticks)
        ax.tick_params(axis='both', which='both', labelsize=FONT_SIZE)
        cbar.ax.set_yticklabels(ticks, fontdict={'fontsize': FONT_SIZE})
