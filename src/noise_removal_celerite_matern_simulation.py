#!/usr/bin/env python
# coding: utf-8

## Evaluate the S+Leaf kernel on the full signal over a range of gap sizes

# - Use Celerite2
# - Pulsation + noise (S+Leaf does not work on the Eclipsing Binary signal !!)
# - Matern32 kernel
import logging
import os
import time
from collections import namedtuple

import numpy as np

# Use Celerite v2
import celerite2
from celerite2 import terms

from experiment_util import calculate_error, store_results_in_excel, normalize_mean

np.random.seed(0)

import pandas as pd
pd.set_option('display.precision', 0)
pd.set_option('display.float_format', lambda x: '%.16f' % x)

TIME_SERIES = [f"./simulations/sim_{c:0{3}}_sc.dat" for c in range(0,30)]
# 11300 is the complete first envelope of the test set, use the same number
# here to limit the scope (and time comparison)
N_samples = 11300

def process_results():
    results = []

    for c in range(0, 30):
        time_series = f"./simulations/sim_{c:0{3}}_sc.dat"
        # file 011 is missing
        if not os.path.exists(time_series):
            time_series = f"./simulations/sim_{c:0{3}}_lc.dat"
            if not os.path.exists(time_series):
                logging.warning(f'Skipping file {time_series}')
                continue

        logging.info(f"Processing file {time_series}")

        for noise in [True, False]:
            # Load the data and extract the relevant subset
            df = pd.read_csv(time_series, sep=' ', header = None, names=['Time', 'LC', 'N'],
                             index_col=False, float_precision='round_trip')
            X, Y, N = df['Time'].tolist(), df['LC'].tolist(), df['N'].tolist()
            X, Y, N = np.array(X), np.array(Y), np.array(N)

            # For simulations, we have to take all the data as we don't know where the
            # big gaps are.
            X_part = X[:N_samples]
            Y_part = Y[:N_samples]
            N_part = N[:N_samples]
            YpN_part = Y_part + N_part

            # With noise or without noise
            Y_part = YpN_part if noise else Y_part

            # normalize the data
            _, _, Y_part = normalize_mean(Y_part)

            start_time = time.time()

            # Train the model
            gp = setup_model(X_part)

            gp = train(X_part, Y_part, gp)

            # Fit the model taking into account the observations
            Y_part_pred = evaluate(X=X_part, Y=Y_part, gp=gp)
            execution_time = time.time() - start_time

            error = calculate_error(Y_part, Y_part_pred)

            # Store the results
            Results = namedtuple('Results', ['time_series', 'noise', 'total_length', 'sample_length', 'correlation', 'rmse', 'run_time'])
            r = Results(time_series, noise, len(X), N_samples, round(error.correlation, 4), round(error.rmse, 4),
                        round(execution_time))
            results += [r]

            logging.info(f' Correlation: {r.correlation:0.4f} RMSE: {r.rmse:0.4f}')

    return results


def train(X: np.ndarray, Y: np.ndarray, gp: celerite2.numpy.GaussianProcess):
    from scipy.optimize import minimize

    yerr = 1

    def set_params(params, gp, t):
        gp.mean = params[0]
        theta = np.exp(params[1:])
        gp.kernel = terms.Matern32Term(sigma=theta[0], rho=theta[1], eps=theta[2])
        gp.compute(t, diag=yerr ** 2 + theta[3], quiet=True)
        return gp

    def neg_log_like(params, gp, t, y):
        gp = set_params(params, gp, t)
        return -gp.log_likelihood(y)

    # [mean, sigma1, rho1, tau1, sigma2, rho2, ]
    # Q2=0.25
    initial_params = [0.0, 0.0, 0.0, np.log(10.0), np.log(0.01)]
    soln = minimize(neg_log_like, initial_params, method="L-BFGS-B",
                    args=(gp, X, Y))
    gp = set_params(soln.x, gp, X)
    return gp


def setup_model(X):
    # Initialize the Celerite model
    # Matern 3/2 term
    kernel = terms.Matern32Term(sigma=1.0, rho=1.0, eps=0.01)

    # Setup the GP
    gp = celerite2.GaussianProcess(kernel, mean=0.0)
    gp.compute(X, yerr=0)
    return gp


def evaluate(X: np.ndarray, Y: np.ndarray, gp: celerite2.numpy.GaussianProcess):
    """
    Predict the observations at timestamps X, condition on observations Y.
    :param X:
    :param Y:
    :param cov_matrix:
    :return:
    """
    # We now use Celerite to predict the missing data on the training data scope
    Y_pred, var_pred = gp.predict(Y, t=X, return_var=True)

    return Y_pred


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s')  # include timestamp
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("boto3").setLevel(logging.WARNING)
logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger("s3transfer").setLevel(logging.WARNING)


if __name__ == '__main__':
    results = process_results()
    store_results_in_excel("results/noise_removal_celerite_matern_simulation-<DATETIME>.xlsx", results)