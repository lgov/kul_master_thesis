#!/usr/bin/env python
# coding: utf-8

# ## Evaluate the S+Leaf kernel on the full signal over a range of gap sizes

# - Use S+Leaf
# - Pulsation + noise (S+Leaf does not work on the Eclipsing Binary signal !!)
# - Soho kernel(s)
# - Predefined gap timestamps
# - Multiple gap sizes
import logging
import time
from collections import namedtuple

import torch
import gpytorch
import numpy as np

from experiment_util import calculate_error, store_results_in_excel, normalize_mean

np.random.seed(0)

import pandas as pd
pd.set_option('display.precision', 0)
pd.set_option('display.float_format', lambda x: '%.16f' % x)

TIME_SERIES = ["./data/sim_pulse.lc" , "./data/sim_noisy_pulse.lc"]
GAP_IDCS = [1100, 3500, 5500, 6500, 10800]
GAP_SIZES = [10, 50, 100, 200, 400, 800]
# 11300 is the complete first envelope
N_part = 11300


def process_results():
    results = []

    for time_series in TIME_SERIES:
        # Load the data and extract the relevant subset
        df = pd.read_csv(time_series, sep=' ', header=None, names=['Time', 'Lc'],
                         index_col=False, float_precision='round_trip')
        X, Y = df['Time'].tolist(), df['Lc'].tolist()
        X, Y = np.array(X), np.array(Y)

        # Extract the first envelope
        X_part = X[:N_part]
        Y_part = Y[:N_part]

        _, _, Y_part = normalize_mean(Y_part)

        for gap_size in GAP_SIZES:
            logging.info(f"Processing file {time_series}, gap size {gap_size}")

            Y_mask = np.zeros(X_part.shape)
            # Do all gaps at once:
            for gap_idx in GAP_IDCS:
                # Define the masks where will introduce the gaps
                Y_mask[int(gap_idx - gap_size / 2):int(gap_idx + gap_size / 2)] = 1

            # Now store the timestamps of this gaps for our fitting procedure
            X_gaps = X_part[Y_mask == 1]
            # And remove the observations at the gaps from X and Y
            X_without_gaps = X_part[Y_mask == 0]
            Y_without_gaps = Y_part[Y_mask == 0]

            X_without_gaps = torch.tensor(X_without_gaps)
            Y_without_gaps = torch.tensor(Y_without_gaps)
            X_gaps = torch.tensor(X_gaps)
            X_part = torch.tensor(X_part)
            Y_part = torch.tensor(Y_part)

            start_time = time.time()
            # Go through the training + evaluation loop
            likelihood, model = setup_model(X_without_gaps, Y_without_gaps)

            train(X_without_gaps, Y_without_gaps, likelihood, model)

            Y_pred_gaps = evaluate(X_part, likelihood, model)
            execution_time = time.time() - start_time

            error = calculate_error(Y_part.cpu(), Y_pred_gaps.mean.cpu())

            del model
            del likelihood

            # Store the results
            Results = namedtuple('Results', ['time_series', 'gap_size', 'correlation', 'rmse', 'run_time'])
            r = Results(time_series, gap_size, round(error.correlation, 4), round(error.rmse, 4),
                        round(execution_time))
            results += [r]

            logging.info(f' Correlation: {r.correlation:0.4f} RMSE: {r.rmse:0.4f}')

    return results


def setup_model(X, Y):
    # We will use the simplest form of GP model, exact inference
    class ExactGPModel(gpytorch.models.ExactGP):
        def __init__(self, train_x, train_y, likelihood):
            super(ExactGPModel, self).__init__(train_x, train_y, likelihood)
            self.mean_module = gpytorch.means.ZeroMean()
            # Periodic kernel for the Pulsations
            self.pulse_kernel = gpytorch.kernels.PeriodicKernel()
            self.pulse_kernel.period_length = 1.77
            self.pulse_kernel.lengthscale = 0.001
            # Periodic kernel for the Gaps
            self.pulse_kernel2 = gpytorch.kernels.PeriodicKernel()
            self.pulse_kernel2.period_length = 1.77 * 3
            self.pulse_kernel2.lengthscale = 0.001
            # RBF kernel for the uncorrelated noise
            self.noise_kernel = gpytorch.kernels.RBFKernel()

            # No solution yet for the non-stationary instrumentation noise
            self.covar_module = gpytorch.kernels.AdditiveKernel(
                gpytorch.kernels.ScaleKernel(self.pulse_kernel),
                gpytorch.kernels.ScaleKernel(self.pulse_kernel2),
                gpytorch.kernels.ScaleKernel(self.noise_kernel),
            )

        def forward(self, x):
            mean_x = self.mean_module(x)
            covar_x = self.covar_module(x)
            return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)

    # initialize likelihood and model
    likelihood = gpytorch.likelihoods.GaussianLikelihood()
    model = ExactGPModel(X, Y, likelihood)
    return likelihood, model


def train(X, Y, likelihood, model):
    # As we are using float64 data, we need to convert our model to double
    _ = model.double()
    _ = likelihood.double()
    training_iter = 50
    # Find optimal model hyperparameters
    model.train()
    likelihood.train()
    # Use the adam optimizer on a subset of the hyperparameters
    all_params = set(model.parameters())
    final_params = all_params
    optimizer = torch.optim.Adam(final_params, lr=0.15)
    # "Loss" for GPs - the marginal log likelihood
    mll = gpytorch.mlls.ExactMarginalLogLikelihood(likelihood, model)
    for i in range(training_iter):
        # Zero gradients from previous iteration
        optimizer.zero_grad()
        # Output from model
        output = model(X)
        # Calc loss and backprop gradients
        loss = -mll(output, Y)
        loss.backward()
        # print('Iter %d/%d - Loss: %.3f   Period length: %.3f    noise: %.3f' % (
        #     i + 1, training_iter, loss.item(),
        #     model.covar_module.kernels[0].base_kernel.period_length.item(),
        #     model.likelihood.noise.item()
        # ))
        optimizer.step()


def evaluate(X, likelihood, model):
    # Get into evaluation (predictive posterior) mode
    # TODO: NumericalWarning: CG terminated in 1000 iterations with average residual
    # norm 0.06085999195152116 which is larger than the tolerance of 0.01 specified
    # by gpytorch.settings.cg_tolerance. If performance is affected, consider
    # raising the maximum number of CG iterations by running code in a
    # gpytorch.settings.max_cg_iterations(value) context.
    # https://github.com/cornellius-gp/gpytorch/issues/1220
    model.eval()
    likelihood.eval()
    with torch.no_grad(), gpytorch.settings.fast_pred_var():
        Y_pred_gaps = likelihood(model(X))
    return Y_pred_gaps


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s')  # include timestamp
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("boto3").setLevel(logging.WARNING)
logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger("s3transfer").setLevel(logging.WARNING)


if __name__ == '__main__':
    results = process_results()
    store_results_in_excel("results/simple_fit_gpytorch_rbf_experiment-<DATETIME>.xlsx",
                           results)