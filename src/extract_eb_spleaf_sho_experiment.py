#!/usr/bin/env python
# coding: utf-8

## Evaluate the S+Leaf kernel on the full signal over a range of gap sizes

# - Use S+Leaf
# - Pulsation + noise (S+Leaf does not work on the Eclipsing Binary signal !!)
# - Soho kernel(s)
import logging
import time
from collections import namedtuple

import numpy as np

from experiment_util import calculate_error, store_results_in_excel, normalize_mean

np.random.seed(0)

import pandas as pd
pd.set_option('display.precision', 0)
pd.set_option('display.float_format', lambda x: '%.16f' % x)

from spleaf import cov, term
from scipy.optimize import fmin_l_bfgs_b

TIME_SERIES = [("./data/sim_pulse_eb.lc", "./data/sim_pulse.lc"),
               ("./data/sim_noisy_pulse_eb.lc", "./data/sim_noisy_pulse.lc")]
# 11300 is the complete first envelope
N_part = 11300


def negloglike(x, y, cov_matrix, params_to_fit):
    cov_matrix.set_param(x, params_to_fit)
    nll = -cov_matrix.loglike(y)
    # gradient
    nll_grad = -cov_matrix.loglike_grad()[1]
    return nll, nll_grad


def process_results():
    results = []

    for eb_time_series, no_eb_time_series in TIME_SERIES:
        # Load the data and extract the relevant subset
        df = pd.read_csv(eb_time_series, sep=' ', header=None, names=['Time', 'Lc'],
                         index_col=False, float_precision='round_trip')
        X, Y = df['Time'].tolist(), df['Lc'].tolist()
        X, Y = np.array(X), np.array(Y)

        # Extract the first envelope
        X_part = X[:N_part]
        Y_part = Y[:N_part]

        # normalize the data
        _, _, Y_part = normalize_mean(Y_part)

        start_time = time.time()

        # Train the model
        cov_matrix, params_to_fit = setup_model(X_part)

        train(Y_part, cov_matrix, params_to_fit)

        # Fit the model taking into account the observations
        Y_part_pred = evaluate(X=X_part, Y=Y_part, cov_matrix=cov_matrix)
        execution_time = time.time() - start_time

        # Compare with the light curve without the EB.
        df = pd.read_csv(no_eb_time_series, sep=' ', header=None,
                         names=['Time', 'Lc'], index_col=False,
                         float_precision='round_trip')
        X_orig_pulse, Y_orig_pulse = df['Time'].tolist(), df['Lc'].tolist()
        X_orig_pulse, Y_orig_pulse = np.array(X_orig_pulse), np.array(Y_orig_pulse)
        X_orig_pulse = X_orig_pulse[:N_part]
        Y_orig_pulse = Y_orig_pulse[:N_part]
        # normalize the data
        _, _, Y_orig_pulse = normalize_mean(Y_orig_pulse)

        error = calculate_error(Y_orig_pulse, Y_part_pred)

        # Store the results
        Results = namedtuple('Results', ['time_series', 'gap_size', 'correlation', 'rmse', 'run_time'])
        r = Results(eb_time_series, 0, round(error.correlation, 4), round(error.rmse, 4),
                    round(execution_time))
        results += [r]

        logging.info(f' Correlation: {r.correlation:0.4f} RMSE: {r.rmse:0.4f}')

    return results


def train(Y, cov_matrix, params_to_fit):
    # We now fit the hyperparameters using the fmin_l_bfgs_b function from
    # scipy.optimize.
    xbest, _, _ = fmin_l_bfgs_b(negloglike, cov_matrix.get_param(),
                                args=(Y, cov_matrix, params_to_fit))
    # We now use S+LEAF to predict the missing data on the training data scope
    cov_matrix.set_param(xbest, params_to_fit)


def setup_model(X):
    # Initialize the S+LEAF model
    # Two stochastically-driven harmonic oscillator
    # One Uncorrelated measurement errors (yerr)
    cov_matrix = cov.Cov(X,
                         err=term.Error(1),
                         sho=term.SHOKernel(sig=-0.04314, P0=1.7474, Q=8019.36505))
    # List of parameters to fit: all (for now)
    params_to_fit = cov_matrix.param
    return cov_matrix, params_to_fit


def evaluate(X, Y, cov_matrix):
    """
    Predict the observations at timestamps X, condition on observations Y.
    :param X:
    :param Y:
    :param cov_matrix:
    :return:
    """
    Y_pred, var_pred = cov_matrix.conditional(Y, X, calc_cov='diag')
    return Y_pred


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s')  # include timestamp
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("boto3").setLevel(logging.WARNING)
logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger("s3transfer").setLevel(logging.WARNING)


if __name__ == '__main__':
    results = process_results()
    store_results_in_excel("results/extract_eb_spleaf_sho_experiment-<DATETIME>.xlsx", results)