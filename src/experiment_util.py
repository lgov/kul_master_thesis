from collections import namedtuple
from datetime import datetime

import pandas as pd
import numpy as np


def calculate_error(Y:np.ndarray, Y_pred:np.ndarray):
    """
    Calculate the correlation between Y and Y_pred, but one-dimensional numpy arrays

    :param Y:
    :param Y_pred:
    :return:
    """
    ts_orig = pd.DataFrame()
    ts_orig['signal'] = Y
    ts_fitted = pd.DataFrame()
    ts_fitted['signal'] = Y_pred
    correlation = ts_orig.corrwith(ts_fitted)
    correlation = correlation['signal'].item()

    rmse =np.linalg.norm(Y - Y_pred) / np.sqrt(len(Y))

    TimeSeriesSimilarity = namedtuple('TimeSeriesSimilarity', ['correlation', 'rmse'])
    res = TimeSeriesSimilarity(correlation, rmse)
    return res


def fill_in_datetime(path, year_first=False):
    """
    Replaces <DATE> or <DATETIME> in a string with current date or
    current date-hour-minutes.
    :param path: string containing <DATE> or <DATETIME>
    :return: path with placeholders replaced
    """

    if not year_first:
       now_date_string = datetime.now().strftime("%d%m%Y")
       now_datetime_string = datetime.now().strftime("%d%m%Y-%H%M")
    else:
       now_date_string = datetime.now().strftime("%Y%m%d")
       now_datetime_string = datetime.now().strftime("%Y%m%d-%H%M")
    path = path.replace("<DATE>", now_date_string)
    path = path.replace("<DATETIME>", now_datetime_string)

    return path


def store_results_in_excel(excel_path, results):
    """
    Save the <results> named tuple as a row in the results pandas Dataframe
    :param excel_path:
    :param results:
    :return:
    """
    results = pd.DataFrame(results)
    file_name = fill_in_datetime(excel_path, year_first=True)
    results.to_excel(file_name, index=False)


def normalize_mean(data: np.ndarray):
    """
    Mean-normalize the data <data>
    :param data:
    :return: mean, stddev, data
    """
    mean, std = data.mean(),data.std()
    data = (data - mean) / std
    return mean, std, data


def normalize_median(data: np.ndarray):
    """
    Median-normalize the data <data>
    :param data:
    :return: median, stddev, data
    """

    median = np.median(data)
    data = (data / median) - 1
    std = data.std()
    return median, std, data


def median_sampling_period(x: np.ndarray):
    """
    Takes an array of timestamps and returns the median distance between the timestamps.
    :param x:
    :return:
    """
    __x_periods = np.diff(x, n=1)
    return np.median(__x_periods)


def evenly_spaced_grid(x: np.ndarray):
    """
    Create a grid of evenly spaced timestamps using the median sampling period of <x>
    between the first and last timestamp in <x>.

    :param x:
    :return:
    """
    msp = median_sampling_period(x)
    # Predict evenly spaced points on our time-axis
    return np.arange(x.min(), x.max(), msp)


def generate_random_noise(Y: np.ndarray):
    """
    Generates vectors <N> and <Y+N> where N is random noise with amplitude uniformly
    sampled between Y.max / 30 and Y.max / 10.
    :param Y:
    :return: <N>, <Y+N>
    """
    yerr = np.random.uniform(Y.max() / 30, Y.max() / 10, len(Y))
    N = yerr * np.random.randn(len(Y))
    return N, Y+N
