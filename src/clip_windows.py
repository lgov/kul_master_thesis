import numpy as np
import torch


def find_nearest_idx(array, value):
    """
    Find the index of the element in <array> that is nearest in value to <value>
    :param array:
    :param value:
    :return:
    """
    idx = np.searchsorted(array, value, side="left")
    return idx


def clip_periodic_windows(X, Y, eclipse_duration,
                          eclipse_period, start_prim_eclipse_date,
                          start_sec_eclipse_date):
    """
    Remove observations of the arrays X and Y and regular intervals

    Returns X' and Y' with observations removed.
    """
    max_date = X.max()

    # Find the next eclipse. Loop over dates here, as the positions in the array will
    # change when we start deleting windows
    # For the second eclipse, subtract one whole period to make the loop easier to program
    next_prim_eclipse_date, next_sec_eclipse_date = start_prim_eclipse_date, start_sec_eclipse_date - eclipse_period

    X_clipped = X.clone().detach() if torch.is_tensor(X) else X.copy()
    Y_clipped = Y.clone().detach() if torch.is_tensor(Y) else Y.copy()

    # Assume the first eclipse is a primary eclipse
    primary_eclipse = True
    next_eclipse_date = next_prim_eclipse_date

    # Now iterate over the data, first the primary eclipse, then a secondary eclipse
    while next_eclipse_date < max_date:
        # There are gaps in the X-data, so we can not delete a window of x pixels, we have to work based on dates.
        next_eclipse_x_start = find_nearest_idx(X_clipped, next_eclipse_date).item()
        next_eclipse_x_end = next_eclipse_x_start + find_nearest_idx(
            X_clipped[next_eclipse_x_start:], next_eclipse_date + eclipse_duration).item()

        # Now delete the window
        i = {"left": max(int(next_eclipse_x_start), 0),
             "right": max(int(next_eclipse_x_end), 0)}
        X_clipped = torch.cat((X_clipped[:i['left']], X_clipped[i['right']:]))\
            if torch.is_tensor(X) else np.concatenate((X_clipped[:i['left']], X_clipped[i['right']:]))
        Y_clipped = torch.cat((Y_clipped[:i['left']], Y_clipped[i['right']:])) \
            if torch.is_tensor(X) else np.concatenate((Y_clipped[:i['left']], Y_clipped[i['right']:]))

        # Move to the next period
        primary_eclipse = not primary_eclipse
        if primary_eclipse:
            next_prim_eclipse_date += eclipse_period
            next_eclipse_date = next_prim_eclipse_date
        else:
            next_sec_eclipse_date += eclipse_period
            next_eclipse_date = next_sec_eclipse_date

    return X_clipped, Y_clipped


def prepare_masks(X, Y, X_window):
    """
    Assuming a mask <X_window> remove all timestamps and observations of <X> and <Y> at
    timestamps near X_window timestamps

    :param X:
    :param Y:
    :param X_window:
    :return: X', Y_mask, Y'
    """
    # We have all timestamps with observations in X_part
    # X_part_window is X_part minus the masked out eclipses
    # If we want to plot the mask, subtract X_part_window from X_part and assume
    # observations of amplitude 1 at those timestamps
    a = X
    b = X_window

    Y_mask = np.zeros(shape=Y.shape)
    X_mask_idcs = np.searchsorted(a, np.setdiff1d(a, b))
    Y_mask[X_mask_idcs] = 1

    # Now remove the observations at the gaps from X and Y
    X_without_gaps = X[Y_mask == 0]
    Y_without_gaps = Y[Y_mask == 0]

    return X_without_gaps, Y_mask, Y_without_gaps
