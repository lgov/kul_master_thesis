#!/usr/bin/env python
# coding: utf-8

# ## Evaluate the S+Leaf kernel on the full signal over a range of gap sizes

# - Use S+Leaf
# - Pulsation + noise (S+Leaf does not work on the Eclipsing Binary signal !!)
# - Soho kernel(s)
# - Predefined gap timestamps
# - Multiple gap sizes
import logging
import time
from collections import namedtuple
from datetime import datetime

import numpy as np

from experiment_util import calculate_error, store_results_in_excel, normalize_mean

np.random.seed(0)

import pandas as pd
pd.set_option('display.precision', 0)
pd.set_option('display.float_format', lambda x: '%.16f' % x)

from spleaf import cov, term
from scipy.optimize import fmin_l_bfgs_b

TIME_SERIES = ["./data/sim_pulse.lc", "./data/sim_noisy_pulse.lc"]
GAP_IDCS = [1100, 3500, 5500, 6500, 10800]
GAP_SIZES = [10, 50, 100, 200, 400, 800]
# 11300 is the complete first envelope
N_part = 11300


def negloglike(x, y, cov_matrix, params_to_fit):
    cov_matrix.set_param(x, params_to_fit)
    nll = -cov_matrix.loglike(y)
    # gradient
    nll_grad = -cov_matrix.loglike_grad()[1]
    return nll, nll_grad


def process_results():
    results = []

    for time_series in TIME_SERIES:
        # Load the data and extract the relevant subset
        df = pd.read_csv(time_series, sep=' ', header=None, names=['Time', 'Lc'],
                         index_col=False, float_precision='round_trip')
        X, Y = df['Time'].tolist(), df['Lc'].tolist()
        X, Y = np.array(X), np.array(Y)

        # Extract the first envelope
        X_part = X[:N_part]
        Y_part = Y[:N_part]

        # normalize the data
        _, _, Y_part = normalize_mean(Y_part)

        for gap_size in GAP_SIZES:
            logging.info(f"Processing file {time_series}, gap size {gap_size}")

            # Do all gaps at once:
            for gap_idx in GAP_IDCS:
                # Define the masks where will introduce the gaps
                Y_mask = np.zeros(X_part.shape)
                Y_mask[int(gap_idx - gap_size / 2):int(gap_idx + gap_size / 2)] = 1

            # Now store the timestamps of this gaps for our fitting procedure
            X_gaps = X_part[Y_mask == 1]
            # And remove the observations at the gaps from X and Y
            X_without_gaps = X_part[Y_mask == 0]
            Y_without_gaps = Y_part[Y_mask == 0]

            start_time = time.time()

            cov_matrix, params_to_fit = setup_model(X_without_gaps)

            train(Y_without_gaps, cov_matrix, params_to_fit)

            Y_pred_gaps = evaluate(X_part, Y_without_gaps, cov_matrix)
            execution_time = time.time() - start_time

            error = calculate_error(Y_part, Y_pred_gaps)

            # Store the results
            Results = namedtuple('Results', ['time_series', 'gap_size', 'correlation', 'rmse', 'run_time'])
            r = Results(time_series, gap_size, round(error.correlation, 4), round(error.rmse, 4),
                        round(execution_time))
            results += [r]

            logging.info(f' Correlation: {r.correlation:0.4f} RMSE: {r.rmse:0.4f}')

    return results


def train(Y, cov_matrix, params_to_fit):
    # We now fit the hyperparameters using the fmin_l_bfgs_b function from
    # scipy.optimize.
    xbest, _, _ = fmin_l_bfgs_b(negloglike, cov_matrix.get_param(),
                                args=(Y, cov_matrix, params_to_fit))
    # We now use S+LEAF to predict the missing data on the training data scope
    cov_matrix.set_param(xbest, params_to_fit)


def setup_model(X):
    # Initialize the S+LEAF model
    # Two stochastically-driven harmonic oscillator
    # One Uncorrelated measurement errors (yerr)
    # Calibration errors (correlated noise)
    # calib_id = (t//1).astype(int) # One calibration per day
    # caliberr = np.random.uniform(0.5, 1.5, calib_id[-1]+1)
    # yerr_calib = caliberr[calib_id]
    # Calibration errors (correlated noise)
    calib_id = (X // 1).astype(int)  # One calibration per day
    caliberr = np.random.uniform(0.5, 1.5, calib_id[-1] + 1)
    yerr_calib = caliberr[calib_id]
    cov_matrix = cov.Cov(X,
                         err=term.Error(1),
                         sho=term.SHOKernel(sig=-0.04314, P0=1.7474, Q=8019.36505),
                         sho2=term.SHOKernel(sig=-0.04314, P0=1.0, Q=8019.36505)
                         #               calerr=term.CalibrationError(calib_id, yerr_calib)
                         )
    # List of parameters to fit: all (for now)
    params_to_fit = cov_matrix.param
    return cov_matrix, params_to_fit


def evaluate(X, Y, cov_matrix):
    """
    Predict the observations at timestamps X, condition on observations Y.
    :param X:
    :param Y:
    :param cov_matrix:
    :return:
    """
    Y_pred_gaps, var_pred_gaps = cov_matrix.conditional(Y, X, calc_cov='diag')
    return Y_pred_gaps


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s')  # include timestamp
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("boto3").setLevel(logging.WARNING)
logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger("s3transfer").setLevel(logging.WARNING)


if __name__ == '__main__':
    results = process_results()
    store_results_in_excel("results/fill_gaps_spleaf_sho_experiment-<DATETIME>.xlsx", results)