#!/usr/bin/env python
# coding: utf-8

# ## Evaluate the S+Leaf kernel on the full signal over a range of gap sizes

# - Use S+Leaf
# - Pulsation + noise (S+Leaf does not work on the Eclipsing Binary signal !!)
# - Soho kernel(s)
# - Predefined gap timestamps
# - Multiple gap sizes
import logging
import time
from collections import namedtuple

import numpy as np

from experiment_util import calculate_error, store_results_in_excel, normalize_mean

np.random.seed(0)

import pandas as pd
pd.set_option('display.precision', 0)
pd.set_option('display.float_format', lambda x: '%.16f' % x)

# Use Celerite v2
import celerite2
from celerite2 import terms

TIME_SERIES = ["./data/sim_pulse.lc", "./data/sim_noisy_pulse.lc"]
GAP_IDCS = [1100, 3500, 5500, 6500, 10800]
GAP_SIZES = [10, 50, 100, 200, 400, 800]
# 11300 is the complete first envelope
N_part = 11300


def process_results():
    results = []

    for time_series in TIME_SERIES:
        # Load the data and extract the relevant subset
        df = pd.read_csv(time_series, sep=' ', header=None, names=['Time', 'Lc'],
                         index_col=False, float_precision='round_trip')
        X, Y = df['Time'].tolist(), df['Lc'].tolist()
        X, Y = np.array(X), np.array(Y)

        # Extract the first envelope
        X_part = X[:N_part]
        Y_part = Y[:N_part]

        # normalize the data
        _, _, Y_part = normalize_mean(Y_part)

        for gap_size in GAP_SIZES:
            logging.info(f"Processing file {time_series}, gap size {gap_size}")

            # Do all gaps at once:
            for gap_idx in GAP_IDCS:
                # Define the masks where will introduce the gaps
                Y_mask = np.zeros(X_part.shape)
                Y_mask[int(gap_idx - gap_size / 2):int(gap_idx + gap_size / 2)] = 1

            # Now store the timestamps of this gaps for our fitting procedure
            X_gaps = X_part[Y_mask == 1]
            # And remove the observations at the gaps from X and Y
            X_without_gaps = X_part[Y_mask == 0]
            Y_without_gaps = Y_part[Y_mask == 0]

            start_time = time.time()

            # Train the model
            gp = setup_model(X_without_gaps)

            gp = train(X_without_gaps, Y_without_gaps, gp)

            Y_pred_gaps = evaluate(X_part, Y_without_gaps, gp)
            execution_time = time.time() - start_time

            error = calculate_error(Y_part, Y_pred_gaps)

            # Store the results
            Results = namedtuple('Results', ['time_series', 'gap_size', 'correlation', 'rmse', 'run_time'])
            r = Results(time_series, gap_size, round(error.correlation, 4), round(error.rmse, 4),
                        round(execution_time))
            results += [r]

            logging.info(f' Correlation: {r.correlation:0.4f} RMSE: {r.rmse:0.4f}')

    return results


def train(X: np.ndarray, Y: np.ndarray, gp: celerite2.numpy.GaussianProcess):
    from scipy.optimize import minimize

    yerr = 1

    def set_params(params, gp, t):
        gp.mean = params[0]
        theta = np.exp(params[1:])
        gp.kernel = terms.SHOTerm(sigma=theta[0], w0=theta[1], Q=theta[2]) + \
                    terms.SHOTerm(sigma=theta[3], w0=theta[4], Q=theta[5])

        gp.compute(t, diag=yerr ** 2 + theta[6], quiet=True)
        return gp

    def neg_log_like(params, gp, t, y):
        gp = set_params(params, gp, t)
        return -gp.log_likelihood(y)

    # [mean, sigma1, w1, Q1, sigma2, w2, Q2]
    # Q2=0.25
    initial_params = [0.0, 0.0, 0.0, np.log(10.0), 0.0, 0.0, np.log(10.0), np.log(0.01)]
    soln = minimize(neg_log_like, initial_params, method="L-BFGS-B",
                    args=(gp, X, Y))
    gp = set_params(soln.x, gp, X)
    return gp


def setup_model(X):
    # Initialize the Celerite model
    # SHO kernel with the same arguments as for S+Leaf
    kernel = terms.SHOTerm(sigma=0.04314, w0=1.7474, Q=8019.36505) +\
             terms.SHOTerm(sigma=0.04314, w0=1.0, Q=8019.36505)

    # Setup the GP
    gp = celerite2.GaussianProcess(kernel, mean=0.0)
    gp.compute(X, yerr=0)
    return gp


def evaluate(X: np.ndarray, Y: np.ndarray, gp: celerite2.numpy.GaussianProcess):
    """
    Predict the observations at timestamps X, condition on observations Y.
    :param X:
    :param Y:
    :param cov_matrix:
    :return:
    """
    # We now use Celerite to predict the missing data on the training data scope
    Y_pred, var_pred = gp.predict(Y, t=X, return_var=True)

    return Y_pred


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s %(message)s')  # include timestamp
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("requests").setLevel(logging.WARNING)
logging.getLogger("boto3").setLevel(logging.WARNING)
logging.getLogger("botocore").setLevel(logging.WARNING)
logging.getLogger("s3transfer").setLevel(logging.WARNING)


if __name__ == '__main__':
    results = process_results()
    store_results_in_excel("results/fill_gaps_celerite_sho_experiment-<DATETIME>.xlsx", results)